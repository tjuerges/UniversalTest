/*----- PROTECTED REGION ID(UniversalTestClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        UniversalTestClass.h
//
// description : Include for the UniversalTest root class.
//               This class is the singleton class for
//                the UniversalTest device class.
//               It contains all properties and methods which the
//               UniversalTest requires only once e.g. the commands.
//
// project :     UniversalTest
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author:  $
// Copyright (C): 2015-2019
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef UniversalTestClass_H
#define UniversalTestClass_H

#include <tango/tango.h>
#include <string>
#include <vector>
#include "UniversalTest.h"


/*----- PROTECTED REGION END -----*/	//	UniversalTestClass.h


namespace UniversalTest_ns
{
/*----- PROTECTED REGION ID(UniversalTestClass::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	UniversalTestClass::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute setState class definition
class setStateAttrib: public Tango::Attr
{
public:
	setStateAttrib():Attr("setState",
			Tango::DEV_STATE, Tango::READ_WRITE) {};
	~setStateAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<UniversalTest *>(dev))->read_setState(att);}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att)
		{(static_cast<UniversalTest *>(dev))->write_setState(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<UniversalTest *>(dev))->is_setState_allowed(ty);}
};


/**
 *	The UniversalTestClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  UniversalTestClass : public Tango::DeviceClass
#else
class UniversalTestClass : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(UniversalTestClass::Additionnal DServer data members) ENABLED START -----*/


	/*----- PROTECTED REGION END -----*/	//	UniversalTestClass::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;

		//	Method prototypes
		static UniversalTestClass *init(const char *);
		static UniversalTestClass *instance();
		~UniversalTestClass();
		Tango::DbDatum	get_class_property(std::string &);
		Tango::DbDatum	get_default_device_property(std::string &);
		Tango::DbDatum	get_default_class_property(std::string &);

	protected:
		UniversalTestClass(std::string &);
		static UniversalTestClass *_instance;
		void command_factory();
		void attribute_factory(std::vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		std::string get_cvstag();
		std::string get_cvsroot();

	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(std::vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,std::vector<Tango::Attr *> &);
		std::vector<std::string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(std::vector<Tango::Attr *> &att_list, std::string attname);
};

}	//	End of namespace

#endif   //	UniversalTest_H
